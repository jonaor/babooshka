/**
 * Created by Liran on 17-Feb-17.
 */
"use strict";

let utils = require('../scripts/utils');

module.exports.settingsAsObject = function(settings){
    let settingsObj = {};
    if(settings){
        for(let entry of settings){
            settingsObj[entry.name] = entry.value;
        }
    }
    return settingsObj;
};

module.exports.loadSettings = function(settings){
    let settingsObj = module.exports.settingsAsObject(settings);
    let urlParts = getUrlParts(settingsObj.forwardUrl || utils.getEnv('FORWARD_URL'));

    process.env.APP_VERSION = process.env.npm_package_version || utils.getEnv('APP_VERSION') ||'unknown';
    process.env.FORWARD_HOST = urlParts.host || utils.getEnv('FORWARD_HOST');
    process.env.FORWARD_BASE_URL = urlParts.baseUrl || utils.getEnv('FORWARD_BASE_URL') || '';
    process.env.FORWARD_PROTOCOL = settingsObj.forwardProtocol || utils.getEnv('FORWARD_PROTOCOL');
    process.env.ALLOW_UNTRUSTED_CRT = settingsObj.allowSelfSignedCert === '1';
    process.env.ALLOW_CHANNEL_FALL_THROUGH = settingsObj.allowChannelFallThrough === '1';
};

let getUrlParts = function(url){
    let slashIndex = url.indexOf("/");
    return {
        host: slashIndex > 0 ? url.slice(0,slashIndex) : url,
        baseUrl: slashIndex > 0 ? url.slice(slashIndex) : undefined,
    };
};