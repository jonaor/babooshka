"use strict";

let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let database = require('./database/database');
let settings = require('./scripts/settings');

let app = express();

database.getSettings()
    .then(settingsRows=>{
      settings.loadSettings(settingsRows);
    }).catch(e=>{
      console.log(e);
    });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(require('./middlewares/bodyParser'));
app.use(cookieParser());
app.use(require('./middlewares/requestInputValidator'));

app.use(require('./routes/router'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
app.use(function(err, req, res, next) {
  var statusCode = err.status || 500;
  res.status(statusCode);
  if(statusCode === 500){
    console.error(err);
  }
  res.send({
    message: 'BabooshkaError: ' + err.message,
    error: err
  });
});


module.exports = app;
