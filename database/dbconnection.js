"use strict";

/* a component used to abstract database connection, converting the functional
   workflow into a promise based one
 */
var db;

var initialize = function(dbPath, startupScript){
    if(db) return; //already initialized
    var sqlite3 = require('sqlite3').verbose();
    console.log('connecting to database:',dbPath);
    db = new sqlite3.Database(dbPath);
    db.serialize(startupScript(db));
};

/** query command converted to a promise */
var all = function(query, params){
    if(!db) return;
    return new Promise(function(onResult, onError){
        try {
            db.all(query, params, function (err, rows) {
                if (err) {
                    onError(err);
                } else {
                    onResult(rows);
                }
            });
        } catch (err){
            onError(err);
        }
    });
};

/** query command converted into a promise, this method will
 * only return a single entry. If no matching entry is found this
 * method will end with a None error */
var one = function(query, params){
    if(!db) return;
    return new Promise(function(onResult, onError){
        try {
            db.all(query.startsWith('SELECT') ? query +' LIMIT 1' : query, params, function (err, rows) {
                if (err) {
                    onError(err);
                } else if(rows.length > 0){
                    onResult(rows[0]);
                } else {
                    onResult();
                }
            });
        } catch (err){
            onError(err);
        }
    });
};

/** run command converted to a promise */
var run = function(command, params){
    if(!db) return;
    return new Promise(function(onResult, onError){
        try {
            db.run(command, params, function (err) {
                if (err) {
                    onError(err);
                } else {
                    onResult();
                }
            });
        } catch (err){
            onError(err);
        }
    });
};

module.exports.initialize = initialize;
module.exports.all = all;
module.exports.one = one;
module.exports.run = run;
