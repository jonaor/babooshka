"use strict";

const JSON_ERR = 'JsonParseException, ';

var parseJsonBody = function(req, res, next) {
    if(req.hasBody){
        try{
            req.body = JSON.parse(req.body);
            next();
        } catch (e){
            e.message = JSON_ERR.concat(e.message);
            next(e);
        }
    } else {
        next();
    }
};

module.exports =  parseJsonBody;
