"use strict";

var express = require('express');
var router = express.Router();
let db = require('../database/database');

router.get('/',function(req,res,next){
    db.getAllChannels()
        .then(channels=>{
            console.log(JSON.stringify(channels));
            res.send(channels);
        })
        .catch(err=>{
            next(parseSQLError(err));
        });
});

router.delete('/',function(req,res,next){
    if(req.hasQuery && req.query.id){
        db.deleteChannel(req.query.id)
            .then(deletedRows=>{
                res.send({});
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var err = new Error('missing query param "id"');
        err.status = 400;
        next(err);
    }
});

router.put('/',function(req,res,next){
    if(req.hasBody && req.body._id && req.body.channel){
        db.updateChannel(req.body)
            .then(updatedChannel=>{
                res.send({});
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var missingErr;
        if(!req.body){
            missingErr = 'body';
        } else if(!req.body._id){
            missingErr = 'id';
        } else if(!req.body.channel){
            missingErr = 'channel';
        }
        var err = new Error('missing '+missingErr);
        err.status = 400;
        next(err);
    }
});

router.post('/',function(req,res,next){
    if(req.hasBody && req.body.channel){
        db.insertChannel(req.body.channel)
            .then(newChannel=>{
                res.send(newChannel);
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var missingErr;
        if (!req.body){
            missingErr = 'body';
        } else {
            missingErr = 'channel';
        }
        var err = new Error('missing '+missingErr);
        err.status = 400;
        next(err);
    }
});

router.post('/live-indexing',function(req,res,next){
    if(req.hasBody && req.body.channel !== undefined  && req.body.channel > -1 && req.body.index !== undefined){
        db.updateChannelIndexingState(req.body.channel, req.body.index)
            .then(updatedChannel=>{
                res.send(updatedChannel);
            })
            .catch(err=>{
                next(parseSQLError(err));
            });
    } else {
        var missingErr;
        if (!req.body) {
            missingErr = 'body';
        } else if(req.body.channels === undefined) {
            missingErr = 'channel';
        } else if(req.body.channels < 0){
            missingErr = 'channel, default channel cannot be indexed';
        } else {
            missingErr = 'index';
        }
        var err = new Error('missing '+missingErr);
        err.status = 400;
        next(err);
    }
});

function parseSQLError(err) {
    try{
        if(err && err.message){
            if(err.message.includes('SQLITE_CONSTRAINT: UNIQUE')){
                err.message = "A channel with this name already exist";
            }
        }
        err.status = 400;
    } catch (error){
        err = error;
    }
    return err;
}

module.exports = router;
